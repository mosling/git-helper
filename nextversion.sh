#!/bin/bash

BASEDIR=$(dirname "$0")
# shellcheck source=./helper.sh
source "${BASEDIR}/helper.sh"
REMOTE_NAME=origin
isRelease=false
CHECKMARK="${GREEN}\u2713${NOCOLOR}"
PC_HOOK=.git/hooks/pre-commit

exitProcess()
{
    if [[ -f "$PC_HOOK.backup" ]]; then
        mv $PC_HOOK.backup $PC_HOOK
        echo -e "  ${CHECKMARK} activate pre-commit hook ${GREEN}${PC_HOOK}${NOCOLOR}"
    fi

    exit $1
}

updateRemoteBranch() {
    if [[ $# -ne 2 ]]; then
        colorbanner "${RED}" "function updateRemoteBranch need two parameter"
        exitProcess 2
    fi

    colorbanner "${GREEN}" "$2"
    git checkout "$1" --recurse-submodules
    git push
}

updateMainBranch() {
    # at this position switch to main
    MAIN_UPDATE=$(git remote -v update 2>&1 >/dev/null | sed -n '/up to date/ { /'"$PROD_BRANCH"'/p; }' | wc -l)
    if [[ $MAIN_UPDATE -ne 1 ]]; then
        git checkout $PROD_BRANCH >/dev/null 2>&1
        git pull >/dev/null 2>&1
        PULL_RES=$?
        git checkout develop >/dev/null 2>&1
        if [[ $PULL_RES -ne 0 ]]; then
            colorbanner "${RED}" "Production branch $PROD_BRANCH not up to date and can't be updated."
            exitProcess 2
        fi
    fi
    echo -e "  ${CHECKMARK} $PROD_BRANCH up to date with remote"
}

checkOptionalRemoveBranch() {
    TN="refs/tags/$1"
    TAGEXISTS=$(git for-each-ref $TN | wc -l)
    if [[ "$TAGEXISTS" -gt "0" ]]; then
        MAIN_BRANCH=$(git branch --contains $TN | grep $PROD_BRANCH | wc -l)
        if [[ "$MAIN_BRANCH" -eq "1" ]]; then
            colorbanner "${RED}" "Tag $1 exists at the production branch $PROD_BRANCH -- can't proceed."
            exitProcess 2
        else
            read -rp "Remove Existing Branch '$1' [y/N] " delete_branch
            delete_branch=${delete_branch:-N}
            if [[ "$delete_branch" == "y" ]]; then
                git tag -d $1
                if [[ "$REMOTE_NAME" != "local" ]]; then
                    git push --delete $REMOTE_NAME tagname
                fi
            else
                colorbanner "${GREEN}" "Please remove/rename tag $1 and restart."
                exitProcess 2
            fi
        fi
    fi
}

if [[ "help" == $1 || "--help" == $1 || "-h" == $1 ]]; then
    echo -e ${GREEN}
    echo "Please start with $0 [remote-name] and follow the interview."
    echo "  * 'origin' is the default remote-name"
    echo "  * please use 'local' as remote-name without remote connection (i.e. for testing only)"
    echo -e ${NOCOLOR}
    exitProcess 0
elif [[ "local" == "$1" ]]; then
    REMOTE_NAME=local
    colorbanner "${GREEN}" "Create new Release without remote connection used."
elif [[ $# -eq 1 ]]; then
    REMOTE_NAME=$1
fi

if [[ $REMOTE_NAME != "local" ]]; then
    colorbanner "${GREEN}" "Create new Release with remote repository $REMOTE_NAME"
fi

git status >/dev/null
if [ "$?" -eq 128 ]; then
    colorbanner "${RED}" "The current directory isn't part of a git repository --> stop."
    exitProcess 2
else
    echo "preconditions:"
    echo -e "  ${CHECKMARK} git repository"
fi

if [ "local" != "${REMOTE_NAME}" ]; then
    ## check for existing remote connection
    REMOTE_CONNECTION=$(git remote get-url ${REMOTE_NAME} 2>/dev/null)
    if [ "$?" -ne "0" ]; then
        colorbanner "${RED}" "No existing remote connection named $REMOTE_NAME --> stop."
        exitProcess 2
    else
        echo -e "  ${CHECKMARK} remote repository ${GREEN}$REMOTE_CONNECTION${NOCOLOR}"
    fi
fi

GIT_FLOW=$(git flow config 2>/dev/null)
if [ "$?" != 0 ]; then
    colorbanner "${RED}" "The repository must have git flow initialized, please call 'git flow init'."
    exitProcess 2
else
    echo -e "  ${CHECKMARK} git flow activated"
fi

PROD_BRANCH=$(echo $GIT_FLOW | sed -n -e "s/.*production releases: \([^ ]*\).*/\1/p")
if [[ "" == "${PROD_BRANCH}" ]]; then
    colorbanner "${RED}" "Can't find a production branch using 'git flow config'"
    exitProcess 2
fi

if [[ -f .gitmodules ]]; then
    echo -e "  ${CHECKMARK} project has submodules"
fi

if [[ -n $(git status -s) ]]; then
    colorbanner "${RED}" "The branch has changes not on stage."
    exitProcess 2
else
    echo -e "  ${CHECKMARK} all changes staged"
fi

if [[ -f "$PC_HOOK" ]]; then
    mv $PC_HOOK $PC_HOOK.backup
    echo -e "  ${CHECKMARK} deactivate pre-commit hook ${GREEN}${PC_HOOK}${NOCOLOR}"
fi

CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
if [[ "$CURRENT_BRANCH" =~ ^release/.* ]]; then
    NEXT_VERSION=$(echo "${CURRENT_BRANCH}" | sed -n -e 's/release\///p')
    isRelease=true
    echo -e "  ${CHECKMARK} continue with release branch $NEXT_VERSION"
elif [ "$CURRENT_BRANCH" != "develop" ]; then
    colorbanner "${RED}" "You must at branch develop to start a release --> stop."
    exitProcess 2
else
    echo -e "  ${CHECKMARK} on develop branch"
fi

if [ "local" != "${REMOTE_NAME}" ]; then
    if [[ "false" == "$isRelease" ]]; then
        ## check for not pushed changes for develop branch only
        ahead=$(git log --oneline $REMOTE_NAME/develop..HEAD | wc -l)
        if [ "$ahead" -gt 0 ]; then
            colorbanner "${RED}" "Your branch is ahead of '$REMOTE_NAME/develop' by ${ahead} commit(s). --> stop."
            exitProcess 2
        else
            echo -e "  ${CHECKMARK} all commits pushed for develop branch"
        fi
    fi


    if ! git pull >/dev/null 2>&1; then
        if [[ "true" == "$isRelease" ]]; then
            echo -e "  ${CHECKMARK} assume continue at local release branch"
        else
            remoteUrl=$(git remote get-url --push $REMOTE_NAME)
            colorbanner "${RED}" " Can't connect repository at '${remoteUrl}' --> stop."
            exitProcess 2
        fi
    else
        echo -e "  ${CHECKMARK} remote connection exists"
    fi

    updateMainBranch
fi

if [[ "false" == "$isRelease" ]]; then
    VERSION=$(git describe 2> /dev/null)
    if [[ -z "$VERSION" ]]; then
        colorbanner "${BLUE}" " Can't find annotated tag --> search for normal tag"
        VERSION=$(git describe --tags 2> /dev/null)
        if [[ -z "$VERSION" ]]; then
            colorbanner "${BLUE}" " Can't find any tag in the repository --> use 0.0.0"
            VERSION="0.0.0-0-g123456"
        fi
    fi

    echo -e "production branch : ${GREEN}${PROD_BRANCH}${NOCOLOR}"
    echo -e "current version   : ${GREEN}$VERSION${NOCOLOR}"
    # shellcheck disable=SC2001
    V=$(echo $VERSION | sed -e 's/-.*//')

    IFS='.' read -r -a varr <<<"$V"

    declare -a nv
    idx=0
    l=${#varr[@]}
    echo "please select the next release number for the application:"
    cv=""
    for element in "${varr[@]}"; do
        nn=$((element + 1))

        ## adding optional dot
        if [ -n "$cv" ]; then
            cv=$cv.
        fi

        ## adding the value
        vv=$cv$nn

        ## adding .0 for following version parts
        for ((i = 1; i < l; i++)); do
            vv="$vv.0"
        done

        ## store result
        nv[$idx]=$vv

        ## set values for the next round
        l=$((l - 1))
        cv=$cv$element
        idx=$((idx + 1))
    done
    nv[$idx]="input"
    idx=$((idx + 1))
    nv[$idx]="stop"

    createmenu "${nv[@]}"
    NEXT_VERSION=${nv[$(($? - 1))]}

    if [ "input" == "$NEXT_VERSION" ]; then
        read -rp "set next version to --> " NEXT_VERSION
    fi
fi

if [ "stop" != "$NEXT_VERSION" -a "" != "$NEXT_VERSION" ]; then

    checkOptionalRemoveBranch $NEXT_VERSION

    if [ "false" == ${isRelease} ]; then
        colorbanner "${GREEN}" "Start Release $NEXT_VERSION"
        git flow release start "$NEXT_VERSION"
    fi

	read -rp "Finish the Release (y:without squash, Y:squash) [y/Y/N] " finish
    finish=${finish:-N}

    WITH_SQUASH=""
    if [ "Y" == "$finish" ]; then
        WITH_SQUASH=" --squash"
        finish="y"
    fi

    if [ "y" == "$finish" ]; then
        SIGN_KEY=$(git config user.signingkey)

        WITH_SIGN=""
        if [ -n "$SIGN_KEY" ]; then
            WITH_SIGN=" --sign"
        fi

        colorbanner "${GREEN}" "Finish Release${WITH_SQUASH}${WITH_SIGN}"
        git flow release finish $WITH_SQUASH $WITH_SIGN "$NEXT_VERSION"

        if [ "local" != "${REMOTE_NAME}" ]; then
            updateRemoteBranch ${PROD_BRANCH} "Publish ${PROD_BRANCH} with the Release Version"
            updateRemoteBranch develop "Switch Back to Develop Branch and Publish"
        fi
    elif [ "local" != "${REMOTE_NAME}" ]; then
        read -rp "Publish the Release Branch [y/N] " publishRelease
        publishRelease=${publishRelease:-N}

        if [ "y" == "$publishRelease" ]; then
            git flow release publish
        fi
    fi
else
    if [ "stop" == "$NEXT_VERSION" ]; then
        colorbanner "${GREEN}" "Stopped by the User"
    else
        colorbanner "${RED}" "Missing Value for the Next Release."
    fi
fi

exitProcess 0

